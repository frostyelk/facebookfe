/*
 * Copyright (c) 2014 Frosty Elk AB
 */
/* jshint -W117, -W059 */

function GetPluginSettings()
{
	return {
		"name":			"Facebook FE",
		"id":			"FacebookFE",
		"version":		"0.9.1.1",
		"description":	"Access Facebook API features.",
		"author":		"Frosty Elk",
		"help url":		"http://www.frostyelk.se",
		"category":		"Frosty Elk",
		"type":			"object",			// not in layout
		"rotatable":	false,
		"flags":		pf_singleglobal,
		"dependency":	"channel.html;facebookadapter.js"
	};
}

//////////////////////////////////////////////////////////////
// Conditions
AddCondition(0,	0, "Is ready", "Facebook", "Is ready", "True when the Facebook API has loaded and is ready to be used.", "IsReady");
AddCondition(1,	0, "Is user logged in", "Facebook", "Is user logged in", "True if currently being viewed inside Facebook by a logged in user.", "IsLoggedIn");

AddCondition(2, cf_trigger, "On user logged in", "Facebook", "On user logged in", "Triggered when the user successfully logs in.", "OnLogIn");
AddCondition(3, cf_trigger, "On user logged out", "Facebook", "On user logged out", "Triggered when the user logs out.", "OnLogOut");
AddCondition(4, cf_trigger, "On name available", "Facebook", "On name available", "Triggered after login when the user's name becomes available.", "OnNameAvailable");

AddCondition(5, cf_trigger, "On user top score available", "Scores (when logged in with permission)", "On user top score available", "Triggered after the 'request user top score' action.", "OnUserTopScoreAvailable");

AddCondition(6, cf_trigger, "On hi-score", "Scores (when logged in with permission)", "On hi-score", "Triggered for each top score after the 'request hi-score board' action.", "OnHiscore");

AddCondition(7, cf_trigger, "On score submitted", "Scores (when logged in with permission)", "On score submitted", "Triggered when the 'publish score' action completes.", "OnScoreSubmitted");

AddCondition(8,	cf_trigger, "On ready", "Facebook", "On ready", "Triggered when the Facebook API has loaded and is ready to be used.", "OnReady");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different App Requests.", "\"\"");
AddCondition(9,	cf_trigger, "On App Request Successful", "Facebook FE", "On App Request Sucessful <b>{0}</b>", "Triggered when the App Request is successful.", "OnAppRequestSuccess");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different App Requests.", "\"\"");
AddCondition(10, cf_trigger, "On App Request Failure", "Facebook FE", "On App Request Failure <b>{0}</b>", "Triggered when the App Request has failed.", "OnAppRequestFailure");

AddStringParam("Object Id", "The object id recieved from other user", "\"\"");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different Get user Objects.", "\"\"");
AddCondition(11, cf_trigger, "On sent user object available", "Facebook FE", "On user object <i>{0}</i> available with tag <b>{1}</b>", "Triggered when the a sent user object is available.", "OnUserObjectAvailable");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different Get user Objects.", "\"\"");
AddCondition(12, cf_trigger, "On user objects NOT available", "Facebook FE", "On user objects NOT available for tag <b>{0}</b>", "Triggered when the no objects are available.", "OnUserObjectsNotAvailable");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different Get Friends.", "\"\"");
AddCondition(13, cf_trigger, "On Friends available", "Facebook FE", "On Friends available for tag <b>{0}</b>", "Triggered when the list of Friends are available.", "OnFriendsAvailable");

AddStringParam("Object Id", "The object id asked for from other user", "\"\"");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different Get user Objects.", "\"\"");
AddCondition(14, cf_trigger, "On user object asked for", "Facebook FE", "On user ask for object <i>{0}</i> received with tag <b>{1}</b>", "Triggered when the ask for user object is available.", "OnUserObjectAskedFor");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different actions.", "\"\"");
AddCondition(15, cf_trigger, "On additional permissions approved", "Facebook FE", "On permissions approved. Tag <b>{0}</b>", "Triggered when the ask for more permissions is approved.", "OnAskForMorePermissionsApproved");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different actions.", "\"\"");
AddCondition(16, cf_trigger, "On additional permissions rejected", "Facebook FE", "On permissions rejected. Tag <b>{0}</b>", "Triggered when the ask for more permissions is rejected.", "OnAskForMorePermissionsRejected");



//////////////////////////////////////////////////////////////
// Actions
AddComboParamOption("None");
AddComboParamOption("Publish to stream");
AddComboParam("Stream permission", "Ask user for permission to automatically (without prompting) publish to the user's stream.  Do not ask for permissions you don't need!");
AddComboParamOption("None");
AddComboParamOption("Publish scores");
AddComboParam("Action permission", "Ask user for permission to publish scores for the user.  The App Secret is required to post scores.  Do not enter the App Secret if you are not using scores.");
AddAction(2, 0, "Log in", "Facebook", "Log in (request <i>{0}</i>, <i>{1}</i>)", "Log the user in to the application so their details can be accessed.", "LogIn");
AddAction(3, 0, "Log out", "Facebook", "Log out", "Log the user out.", "LogOut");

AddAction(4, 0, "Prompt wall post", "Prompt (no permissions required)", "Prompt wall post", "Bring up a dialog where the user can share some text on their wall or choose to cancel.", "PromptWallPost");

AddStringParam("Name", "The name of the app that will appear in the Share dialog.");
AddStringParam("Caption", "The caption appearing beneath the name in the Share dialog.");
AddStringParam("Description", "The description appearing beneath the caption in the Share dialog.");
AddStringParam("Picture URL (optional)", "The URL of an image on your server to use on the Share dialog.  Must include the http://.  Defaults to the exported loading-logo.png.");
AddAction(5, 0, "Prompt to share this app", "Prompt (no permissions required)", "Prompt to share this app (<i>{0}</i>, <i>{1}</i>, <i>{2}</i>, <i>{3}</i>)", "Bring up a dialog where the user can share a link to the app on their wall or choose to cancel.", "PromptToShareApp");

AddStringParam("URL", "The link to share.", "\"http://\"");
AddStringParam("Name", "The link text that will appear in the Share dialog.");
AddStringParam("Caption", "The caption appearing beneath the link text in the Share dialog.");
AddStringParam("Description", "The description appearing beneath the caption in the Share dialog.");
AddStringParam("Picture URL (optional)", "The URL of an image on your server to use on the Share dialog.  Must include the http://");
AddAction(6, 0, "Prompt to share link", "Prompt (no permissions required)", "Prompt to share link <b>{0}</b> (<i>{1}</i>, <i>{2}</i>, <i>{3}</i>, <i>{4}</i>)", "Bring up a dialog where the user can share a link on their wall or choose to cancel.", "PromptToShareLink");

AddStringParam("Message", "The text to publish to the user's feed.  It is recommended to only do this when the user initiates the action.");
AddAction(7, 0, "Publish wall post", "Publish to stream (when logged in with permission)", "Publish wall post with text <i>{0}</i>", "Publish a message to the user's wall.  The user should explicitly initiate this.", "PublishToWall");

AddStringParam("Message", "The text to publish to the user's feed.  It is recommended to only do this when the user initiates the action.");
AddStringParam("URL", "The link to share.", "\"http://\"");
AddStringParam("Name", "The link text.");
AddStringParam("Caption", "The caption appearing beneath the link text.");
AddStringParam("Description", "The description appearing beneath the caption.");
AddStringParam("Picture URL (optional)", "The URL of an image on your server to use on the Share dialog.  Must include the http://");
AddAction(8, 0, "Publish link", "Publish to stream (when logged in with permission)", "Publish link <b>{1}</b> (<i>{0}</i>, <i>{2}</i>, <i>{3}</i>, <i>{4}</i>, <i>{5}</i>)", "Publish a link to the user's wall.  The user should explicitly initiate this.", "PublishLink");

AddNumberParam("Score", "The user's score for this game to publish.");
AddAction(9, 0, "Publish score", "Scores (when logged in with permission)", "Publish score <b>{0}</b>", "Publish a score for this user.", "PublishScore");

AddAction(10, 0, "Request user top score", "Scores (when logged in with permission)", "Request user top score", "Request the user's top score, triggering 'on user top score available' when received.", "RequestUserHiscore");

AddNumberParam("Number of scores", "The number of hi-scores to retrieve.", "10");
AddAction(11, 0, "Request hi-score board", "Scores (when logged in with permission)", "Request hi-score board top {0} scores", "Request the app's hi-score board, triggering 'on hi-score' for each hi-score received.", "RequestHiscores");

AddStringParam("Title", "The title to show in the request.");
AddStringParam("Message", "The message to show in the request.");
AddStringParam("Not used", "For future extension.");
AddStringParam("User Ids", "A list of userids to send the request to, leave empty to use the Friend Picker");
AddComboParamOption("None");
AddComboParamOption("Send");
AddComboParamOption("Ask for");
AddComboParam("Action for objects", "Option to send or ask for objects");
AddStringParam("Object Id", "The Open Graph object ID of the object being sent.");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different App Requests.", "\"\"");
AddAction(12, 0, "App Request", "App Requests", "App request <b>{0}</b> to (<i>{3}</i>). Message <i>{1}</i>. Object action (<i>{4}</i>) on (<i>{5}</i>). Tag: <b>{6}</b>", "Send an App request to the specfied people", "PromptAppRequestPeople");

AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different Object requests.", "\"\"");
AddAction(13, 0, "Get the users outstanding App objects", "App Requests", "Get the users outstanding objects. Tag: <b>{0}</b>", "Get the users outstanding objects(gifts/powerups/etc) sent by other user.", "GetUserAppObjects");

AddStringParam("Object Instance Id", "The instance id received. Stored in FacebookFE.UserRequestInstanceId after a successful get");
// AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different delete requests.", "\"\"");  Tag: <b>{0}</b>
AddAction(14, 0, "Delete a users object", "App Requests", "Delete the users outstanding object:(<i>{0}</i>).", "Delete users outstanding object(gifts/powerups/etc) sent by other user.", "DeleteUserAppObject");

AddComboParamOption("All");
AddComboParamOption("Has App");
AddComboParam("Filter", "Friend selection");
AddComboParamOption("Array");
AddComboParamOption("Events");
AddComboParam("Return result as", "The way to get the result. Array result is in LastData. Event will trigger once for each friend with info variables loaded.");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different Get Friends.", "\"\"");
AddAction(15, 0, "Get Friends", "Friends", "Get friends with filter <i>{0}</i>. Return result as <i>{1}</i>. Tag: <b>{2}</b>", "Get friends", "GetFriends");

AddStringParam("Permissions", "The additional permissions to ask for.", "\"publish_actions\"");
AddComboParamOption("No");
AddComboParamOption("Yes");
AddComboParam("Ask again", "If asking for permissions after rejection");
AddStringParam("Tag", "A tag, which can be anything you like, to distinguish between different actions.", "\"\"");
AddAction(16, 0, "Ask for permissions", "Facebook", "Ask for additional permissions: <i>{0}</i>. Ask again: <i>{1}</i>. Tag: <b>{2}</b>)", "Ask for more permissions e.g. publish_actions.", "AskForMorePermissions");



//////////////////////////////////////////////////////////////
// Expressions
AddExpression(0, ef_return_string, "Get full name", "Facebook", "FullName", "Get the user's full name, if they are logged in.");
AddExpression(1, ef_return_string, "Get first name", "Facebook", "FirstName", "Get the user's first name, if they are logged in.");
AddExpression(2, ef_return_string, "Get last name", "Facebook", "LastName", "Get the user's last name, if they are logged in.");

AddExpression(3, ef_return_number, "Get score", "Scores (when logged in with permission)", "Score", "Get the score, when in a 'on user top score' or 'on hi-score' event.");
AddExpression(5, ef_return_string, "Get hi-score name", "Scores (when logged in with permission)", "HiscoreName", "Get the name of the user with hi-score, when in an 'on hi-score' event.");
AddExpression(6, ef_return_number, "Get hi-score rank", "Scores (when logged in with permission)", "HiscoreRank", "Get the rank of the hi-score, when in an 'on hi-score' event.");

AddExpression(7, ef_return_number, "Get user ID", "Facebook", "UserID", "Get the user's ID, which is different even for users with the same name.");

AddExpression(8, ef_return_number, "Get hi-score user ID", "Scores (when logged in with permission)", "HiscoreUserID", "Get the user ID of the user with hi-score, when in an 'on hi-score' event.");

AddExpression(9, ef_return_string, "Get last data", "Facebook FE", "LastData", "Get the data returned by the last request.");

AddExpression(10, ef_return_string, "Get App Req Response Object Id", "Facebook FE", "AppReqResponseObjectId", "Get the response object returned by the last successful App request.");
AddExpression(11, ef_return_string, "Get the Friends list from the App Request", "Facebook FE", "AppReqResponseTo", "Get the list of friends returned by the last successful App request.");
AddExpression(12, ef_return_number, "Get number of Friends selected in App Request", "Facebook FE", "AppReqResponseToNumb", "Get the number of friends selected in the last successful App request.");

AddExpression(13, ef_return_string, "Get User Request Instance Id", "Facebook FE", "UserRequestInstanceId", "Get the user request instance id. Could be used to delete the object from FB");
//AddExpression(14, ef_return_string, "Get User Request Data", "Facebook FE", "UserRequestData", "Get the Data that was sent for this request. Can be used to sync send and recieve"); // Data parameter used as id
AddExpression(15, ef_return_number, "Get User Request Object Id", "Facebook FE", "UserRequestObjectId", "Get FB object id (request/gift/powerup). This is the id for the object in FB object browser");
AddExpression(16, ef_return_string, "Get User Request From Id", "Facebook FE", "UserRequestFromId", "Get the friends userid that sent this object. Can be used to query more info about the friend.");
AddExpression(17, ef_return_string, "Get User Request From Name ", "Facebook FE", "UserRequestFromName", "Get the friends full name that sent this object.");

//////////////////////////////////////////////////////////////
ACESDone();

// Property grid properties for this plugin
var property_list = [
	new cr.Property(ept_text,		"App ID",		"",		"The App ID Facebook gives you after creating an app."),
	new cr.Property(ept_text,		"App secret",	"",		"The App Secret Facebook gives you after creating an app.  Only necessary for submitting scores!"),
    new cr.Property(ept_combo,		"FB API",       "v2.2",  "The FB API this App should be running with. Apps created after April 2014 are using API 2.x", "v1.0|v2.0|v2.1|v2.2"),
    new cr.Property(ept_combo,		"Support Parse","Yes",  "Add some special handling for Parse integration", "Yes|No") 
	];
	
// Called by IDE when a new object type is to be created
function CreateIDEObjectType() {
	return new IDEObjectType();
}

// Class representing an object type in the IDE
function IDEObjectType() {
	assert2(this instanceof arguments.callee, "Constructor called as a function");
}

// Called by IDE when a new object instance of this type is to be created
IDEObjectType.prototype.CreateInstance = function (instance) {
	return new IDEInstance(instance, this);
};

// Class representing an individual instance of an object in the IDE
function IDEInstance(instance, type) {
	assert2(this instanceof arguments.callee, "Constructor called as a function");

	// Save the constructor parameters
	this.instance = instance;
	this.type = type;

	// Set the default property values from the property table
	this.properties = {};

	for (var i = 0; i < property_list.length; i++) {
		this.properties[property_list[i].name] = property_list[i].initial_value;
	}
}

// Called by the IDE after all initialization on this instance has been completed
IDEInstance.prototype.OnCreate = function () {};

// Called by the IDE after a property has been changed
IDEInstance.prototype.OnPropertyChanged = function (property_name) {};

// Called by the IDE to draw this instance in the editor
IDEInstance.prototype.Draw = function (renderer) {};

// Called by the IDE when the renderer has been released (ie. editor closed)
// All handles to renderer-created resources (fonts, textures etc) must be dropped.
// Don't worry about releasing them - the renderer will free them - just null out references.
IDEInstance.prototype.OnRendererReleased = function () {};