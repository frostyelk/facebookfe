/*
* Copyright (c) 2014 Frosty Elk AB
*/
/* jshint -W117 */

assert2(cr, "cr namespace not created");
assert2(cr.plugins_, "cr.plugins_ not created");

/////////////////////////////////////
// Plugin class
cr.plugins_.FacebookFE = function (runtime) {
    "use strict";
    this.runtime = runtime;
};


(function () {
    "use strict";
    var pluginProto = cr.plugins_.FacebookFE.prototype;

    /////////////////////////////////////
    // Object type class
    pluginProto.Type = function (plugin) {
        this.plugin = plugin;
        this.runtime = this.plugin.runtime;
    };

    var typeProto = pluginProto.Type.prototype;

    var fbAppID = "";
    var fbAppSecret = "";
    var fbReady = false;
    var fbLoggedIn = false;
    var fbUserID = "";
    var fbFullName = "";
    var fbFirstName = "";
    var fbLastName = "";
    var fbRuntime = null;
    var fbInst = null;
    var fbAccessToken = "";
    var fbInitOptions = {};

    var fbScore = 0;
    var fbHiscoreName = "";
    var fbHiscoreUserID = 0;
    var fbRank = 0;

    var fbCanPublishStream = false;
    var fbCanPublishAction = false;
    var fbPerms = "";

    var fbLastData = "";
    var fbAppReqResponseObjectId = "";
    var fbAppReqResponseTo = [];
    var fbConditionTag = {};

    // Data from getting the user request objects
    var fbUserRequestInstanceId = "";
    var fbUserRequestObjectId = "";
    var fbUserRequestFromId = "";
    var fbUserRequestFromName = "";

    var fbScriptUrl = "";
    //var fbScriptUrl1 = "https://connect.facebook.net/en_US/all.js"; // Old legacy API
    var fbScriptUrl2 = "https://connect.facebook.com/en_US/sdk.js"; // New April 2014 API

    var fbApiVersion = "";
    var fbSupportParse = false;

    // Supported API versions, should match properties in edittime.js
    var fbSupportedApiVersions = ["v1.0", "v2.0", "v2.1", "v2.2"];

    function onFBLogin() {
        if (!fbLoggedIn) {
            fbLoggedIn = true;
            fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnLogIn, fbInst);

            FB.api('/me', function (response) {
                fbFullName = response["name"];
                fbFirstName = response["first_name"];
                fbLastName = response["last_name"];
                fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnNameAvailable, fbInst);
            });
        }
    }

    function isDeviceCordova() {
        return (typeof cordova !== 'undefined' && typeof facebookConnectPlugin !== 'undefined');
    }

    function isUsingApiVersion2Flow() {
        return fbApiVersion !== 'v1.0';
    }

    typeProto.onCreate = function () {
    };

    /////////////////////////////////////
    // Instance class
    pluginProto.Instance = function (type) {
        this.type = type;
        this.runtime = this.type.runtime;


    };

    var instanceProto = pluginProto.Instance.prototype;

    instanceProto.onCreate = function () {
        if (this.runtime.isDomFree) {
            cr.logexport("[Construct 2] FacebookFE plugin not supported on this platform - the object will not be created");
            return;
        }

        fbAppID = this.properties[0];
        fbAppSecret = this.properties[1];
        //fbApiVersion = this.properties[2] === 0 ? "v1.0" : "v2.0";
        fbApiVersion = fbSupportedApiVersions[this.properties[2]];
        fbSupportParse = this.properties[3] === 0;
        fbRuntime = this.runtime;
        fbInst = this;

        // April 2014 is the Default SDK
        fbScriptUrl = fbScriptUrl2;
        // Parse can not handle the new JS SDK even with v1.0 setup
        // Fix in Parse API not to execute FB.init, handle status option here instead
        //fbScriptUrl = fbSupportParse ? fbScriptUrl1 : fbScriptUrl2;

        window.fbAsyncInit = function () {

            //var channelfile = '//' + location.hostname;
            var pname = location.pathname;

            if (pname.substr(pname.length - 1) !== '/') {
                pname = pname.substr(0, pname.lastIndexOf('/') + 1);
            }

            var alwaysGetStatus = !fbSupportParse;

            fbInitOptions = {
                "appId": fbAppID,
                "version": fbApiVersion,
                "channelURL": '//' + location.hostname + pname + 'channel.html',
                "status": alwaysGetStatus,
                "cookie": true,
                "oauth": true,
                "xfbml": false,
                "frictionlessRequests": true
            };

            // console.log("Calling FB.init from FacebookFE plugin. API version: " + fbApiVersion);
            FB.init(fbInitOptions);

            FB.Event.subscribe('auth.login', function (response) {
                if (response["authResponse"]) {
                    fbUserID = response["authResponse"]["userID"];
                    onFBLogin();
                } else {
                    console.log("Login event without authResponse. response: " + JSON.stringify(response));
                }
            });

            FB.Event.subscribe('auth.logout', function (response) {
                fbLastData = JSON.stringify(response);
                if (fbLoggedIn) {
                    fbLoggedIn = false;
                    fbFullName = "";
                    fbFirstName = "";
                    fbLastName = "";
                    fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnLogOut, fbInst);
                }
            });

            // Workaround for the cordova plugin, need to make sure authData is loaded before calling FB.getLoginStatus
            // TODO: Check if this is needed after plugin version 0.10.1?
            var doGetLoginStatus = function () {
                FB.getLoginStatus(function (response) {
                    if (response["authResponse"]) {
                        fbUserID = response["authResponse"]["userID"];
                        fbAccessToken = response["authResponse"]["accessToken"];
                        onFBLogin();
                    }
                });
            };

            if (isDeviceCordova()) {
                FB["_initCordova"](doGetLoginStatus);
            } else {
                doGetLoginStatus();
            }

            // Delay OnReady event so layout is loaded before trying to do events
            fbRuntime.tickMe(fbInst);

        };

        if (isDeviceCordova()) {
            console.log("Loading Cordova Facebook Adapter");
            window["FacebookAdapterInit"]();
            window.fbAsyncInit();
        } else {
            // Load the SDK asynchronously.  Don't bother if no App ID provided.
            if (fbAppID.length) {
                (function (d) {
                    var js, id = 'facebook-jssdk';
                    if (d.getElementById(id)) {
                        return;
                    }
                    js = d.createElement('script');
                    js.id = id;
                    js.async = true;
                    js.src = fbScriptUrl;
                    d.getElementsByTagName('head')[0].appendChild(js);
                }(document));
            } else {
                console.log("Facebook object: no App ID provided.  Please enter an App ID before using the object.");
            }
        }


    };

    instanceProto.tick = function () {
        // Just do this once after load
        if (fbRuntime.running_layout) {
            fbRuntime.untickMe(fbInst);
            fbReady = true;
            fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnReady, fbInst);
        }
    };


    instanceProto.onLayoutChange = function () {
        if (this.runtime.isDomFree) {
            return;
        }

        // re-trigger 'on login' and 'on name available' as appropriate for the new layout
        if (fbLoggedIn) {
            fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnLogIn, fbInst);
        }

        if (fbFullName.length) {
            fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnNameAvailable, fbInst);
        }
    };


    /**BEGIN-PREVIEWONLY**/
    instanceProto.getDebuggerValues = function (propsections) {
        propsections.push({
            "title": "Facebook FE",
            "properties": [
                {
                    "name": "Last data",
                    "value": fbLastData,
                    "readonly": true
                }
            ]
        });
    };
    /**END-PREVIEWONLY**/


    //////////////////////////////////////
    // Conditions
    function Cnds() {
    }

    Cnds.prototype.IsReady = function () {
        return fbReady;
    };

    Cnds.prototype.OnReady = function () {
        return true;
    };

    Cnds.prototype.IsLoggedIn = function () {
        return fbLoggedIn;
    };

    Cnds.prototype.OnLogIn = function () {
        return true;
    };

    Cnds.prototype.OnAskForMorePermissionsApproved = function (tag) {
        return cr.equals_nocase(tag, fbConditionTag.permissions);
    };

    Cnds.prototype.OnAskForMorePermissionsRejected = function (tag) {
        return cr.equals_nocase(tag, fbConditionTag.permissions);
    };

    Cnds.prototype.OnLogOut = function () {
        return true;
    };

    Cnds.prototype.OnNameAvailable = function () {
        return true;
    };

    Cnds.prototype.OnUserTopScoreAvailable = function () {
        return true;
    };

    Cnds.prototype.OnHiscore = function () {
        return true;
    };

    Cnds.prototype.OnScoreSubmitted = function () {
        return true;
    };

    Cnds.prototype.OnAppRequestSuccess = function (tag) {
        return cr.equals_nocase(tag, fbConditionTag.apprequests);
    };

    Cnds.prototype.OnAppRequestFailure = function (tag) {
        return cr.equals_nocase(tag, fbConditionTag.apprequests);
    };

    Cnds.prototype.OnUserObjectAvailable = function (objectId, tag) {
        return fbUserRequestObjectId === objectId && cr.equals_nocase(tag, fbConditionTag.userobject);
    };

    Cnds.prototype.OnUserObjectAskedFor = function (objectId, tag) {
        return fbUserRequestObjectId === objectId && cr.equals_nocase(tag, fbConditionTag.userobject);
    };

    Cnds.prototype.OnUserObjectsNotAvailable = function (tag) {
        return cr.equals_nocase(tag, fbConditionTag.userobject);
    };


    Cnds.prototype.OnFriendsAvailable = function (tag) {
        return cr.equals_nocase(tag, fbConditionTag.friends);
    };


    pluginProto.cnds = new Cnds();

    //////////////////////////////////////
    // Actions
    function Acts() {
    }

    Acts.prototype.LogIn = function (perm_stream, perm_action) {
        if (this.runtime.isDomFree || !fbReady) {
            return;
        }

        fbCanPublishStream = (perm_stream === 1);
        fbCanPublishAction = (perm_action === 1);

        var perms = [];

        if (isUsingApiVersion2Flow()) {
            perms.push("public_profile,user_friends,email");
        } else {
            perms.push("basic_info,user_friends,email");

            if (fbCanPublishStream) {
                perms.push("publish_stream");
            }
            if (fbCanPublishAction) {
                perms.push("publish_actions");
            }
        }

        fbPerms = perms.join();

        FB.login(function (response) {
            if (typeof response !== 'undefined' && typeof response["authResponse"] !== 'undefined') {
                fbUserID = response["authResponse"]["userID"];
                fbAccessToken = response["authResponse"]["accessToken"];
                onFBLogin();
            }
        }, {
            "scope": fbPerms
        });
    };

    Acts.prototype.AskForMorePermissions = function (permissions, askAgain, tag) {
        if (this.runtime.isDomFree || !fbReady) {
            return;
        }

        fbConditionTag.permissions = "";

        // This is to separate between read permissions during first login
        // and write permissions afterwards
        if (!fbLoggedIn) {
            console.log("Need to be logged in to ask for more permissions");

            fbConditionTag.permissions = tag;
            fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnAskForMorePermissionsRejected, fbInst);
            return;
        }

        var scopeOptions = {
            "scope": permissions
        };

        if (askAgain === 1) {
            scopeOptions["auth_type"] = 'rerequest';
        }

        console.log("scope: " + JSON.stringify(scopeOptions));

        FB.login(function (response) {
            if (typeof response !== 'undefined' && typeof response["authResponse"] !== 'undefined') {
                fbConditionTag.permissions = tag;
                fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnAskForMorePermissionsApproved, fbInst);
            } else {
                fbConditionTag.permissions = tag;
                fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnAskForMorePermissionsRejected, fbInst);
            }
        }, scopeOptions);

    };


    Acts.prototype.LogOut = function () {
        if (this.runtime.isDomFree) {
            return;
        }

        if (fbLoggedIn) {
            FB.logout(function (response) {
            });
        }
    };

    Acts.prototype.PromptWallPost = function () {
        if (this.runtime.isDomFree || !fbLoggedIn) {
            return;
        }

        FB.ui({
            "method": "feed"
        }, function (response) {
            if (!response || response.error) {
                console.warn(response);
            }
        });
    };

    Acts.prototype.PromptToShareApp = function (name_, caption_, description_, picture_) {
        if (this.runtime.isDomFree || !fbLoggedIn) {
            return;
        }

        FB.ui({
            "method": "feed",
            "link": "http://apps.facebook.com/" + fbAppID + "/",
            "picture": picture_,
            "name": name_,
            "caption": caption_,
            "description": description_
        }, function (response) {
            if (!response || response.error) {
                console.warn(response);
            }
        });
    };

    Acts.prototype.PromptToShareLink = function (url_, name_, caption_, description_, picture_) {
        if (this.runtime.isDomFree || !fbLoggedIn) {
            return;
        }

        FB.ui({
            "method": "feed",
            "link": url_,
            "picture": picture_,
            "name": name_,
            "caption": caption_,
            "description": description_
        }, function (response) {
            if (!response || response.error) {
                console.warn(response);
            }
        });
    };

    Acts.prototype.PublishToWall = function (message_) {
        if (this.runtime.isDomFree || !fbLoggedIn) {
            return;
        }

        var publish = {
            "method": 'stream.publish',
            "message": message_
        };

        FB.api('/me/feed', 'POST', publish, function (response) {
            if (!response || response.error) {
                console.warn(response);
            }
        });
    };

    Acts.prototype.PublishLink = function (message_, url_, name_, caption_, description_, picture_) {
        if (this.runtime.isDomFree || !fbLoggedIn) {
            return;
        }

        var publish = {
            "method": 'stream.publish',
            "message": message_,
            "link": url_,
            "name": name_,
            "caption": caption_,
            "description": description_
        };

        if (picture_.length) {
            publish["picture"] = picture_;
        }

        FB.api('/me/feed', 'POST', publish, function (response) {
            if (!response || response.error) {
                console.warn(response);
            }
        });
    };

    Acts.prototype.PublishScore = function (score_) {
        if (this.runtime.isDomFree || !fbLoggedIn) {
            return;
        }

        var scoreAccessToken = isUsingApiVersion2Flow() ? fbAccessToken : fbAppID + "|" + fbAppSecret;

        FB.api('/' + fbUserID + '/scores', 'POST', {
            "score": Math.floor(score_),
            "access_token": scoreAccessToken
        }, function (response) {
            fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnScoreSubmitted, fbInst);

            if (!response || response.error) {
                console.warn(response);
            }
        });
    };

    Acts.prototype.RequestUserHiscore = function () {
        if (this.runtime.isDomFree || !fbLoggedIn) {
            return;
        }

        FB.api('/me/scores', 'GET', {}, function (response) {
            fbScore = 0;
            var arr = response["data"];

            if (!arr) {
                console.warn("Request for user hi-score failed: " + response);
                return;
            }

            var i, len = arr.length;
            for (i = 0; i < len; i++) {
                if (arr[i]["score"] > fbScore) {
                    fbScore = arr[i]["score"];
                }
            }

            fbLastData = JSON.stringify(arr);
            fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnUserTopScoreAvailable, fbInst);

            if (!response || response.error) {
                console.warn(response);
            } else {
                console.log(response);
            }
        });
    };

    Acts.prototype.RequestHiscores = function (n) {
        if (this.runtime.isDomFree || !fbLoggedIn) {
            return;
        }

        FB.api('/' + fbAppID + '/scores', 'GET', {}, function (response) {

            var arr = response["data"];

            if (!arr) {
                console.warn("Hi-scores request failed: " + response);
                return;
            }

            arr.sort(function (a, b) {
                // descending order
                return b["score"] - a["score"];
            });

            var i, len = Math.min(arr.length, n);

            for (i = 0; i < len; i++) {
                fbLastData = JSON.stringify(arr[i]);
                fbScore = arr[i]["score"];
                fbHiscoreName = arr[i]["user"]["name"];
                fbHiscoreUserID = arr[i]["user"]["id"];
                fbRank = i + 1;
                fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnHiscore, fbInst);
            }

            if (!response || response.error) {
                console.warn(response);
            } else {
                log(response);
            }
        });
    };

    Acts.prototype.PromptAppRequestPeople = function (title_, message_, data_, to_, objectAction_, objectId_, tag_) {
        if (this.runtime.isDomFree || !fbLoggedIn) {
            return;
        }

        fbConditionTag.apprequests = "";
        var actionType = "";

        if (0 === objectAction_) { // "None"
            actionType = "";
            objectId_ = "";
        } else if (1 === objectAction_) { // "Send"
            actionType = "send";
        } else if (2 === objectAction_) { // "Ask for"
            actionType = "askfor";
        }

        FB.ui({
            "method": "apprequests",
            "title": title_,
            "message": message_,
            "data": objectId_,
            //				"data": data_, // Need to use the data field to send the object id.
            "action_type": actionType,
            "object_id": objectId_,
            "to": to_
        }, function (response) {
            fbLastData = JSON.stringify(response);

            // TODO: If the user cancel the UI (in FB Canvas), the response value is an empty array "[]"
            if (!response || response["error"] || response["error_code"] || response === [] ) {
                console.log("Apprequest response failed: " + fbLastData);

                fbConditionTag.apprequests = tag_;
                fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnAppRequestFailure, fbInst);
            } else {
                console.log("Apprequest response successful: " + fbLastData);

                fbAppReqResponseObjectId = response["request"];
                fbAppReqResponseTo = response["to"];
                fbConditionTag.apprequests = tag_;
                fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnAppRequestSuccess, fbInst);
            }
        });
    };


    Acts.prototype.GetUserAppObjects = function (tag_) {
        if (this.runtime.isDomFree || !fbLoggedIn) {
            return;
        }

        fbConditionTag.userobject = "";

        var fields = {
            "fields": "id, application, to, from, data, message, action_type, object, created_time"
        };
        FB.api('/me/apprequests', 'GET', fields, function (response) {

            var arr = response["data"];

            if (!response || response.error || !arr || arr.length === 0) {
                fbLastData = JSON.stringify(response);
                console.log(fbLastData);
                fbConditionTag.userobject = tag_;
                fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnUserObjectsNotAvailable, fbInst);
            } else {
                log(response);

                var i, len = arr.length;
                for (i = 0; i < len; i++) {
                    if (fbAppID === arr[i]["application"]["id"]) {
                        fbLastData = JSON.stringify(arr[i]);
                        fbUserRequestInstanceId = arr[i]["id"];
                        fbUserRequestObjectId = arr[i]["data"];
                        if (!isDeviceCordova()) {
                            fbUserRequestFromId = arr[i]["from"]["id"];
                            fbUserRequestFromName = arr[i]["from"]["name"];
                        }

                        if (arr[i]["action_type"] === "send") {
                            fbConditionTag.userobject = tag_;
                            fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnUserObjectAvailable, fbInst);
                        } else if (arr[i]["action_type"] === "askfor") {
                            fbConditionTag.userobject = tag_;
                            fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnUserObjectAskedFor, fbInst);
                        }
                    }
                }
            }
        });
    };

    Acts.prototype.DeleteUserAppObject = function (objectId /*, tag_*/) {
        if (this.runtime.isDomFree || !fbLoggedIn || "" === objectId) {
            return;
        }

        // fbConditionTag.xxx = "";

        // Need to add access token at Delete
        var params = {
            "access_token": fbAccessToken
        };

        FB.api('/' + objectId, 'delete', params, function (response) {

            if (!response || response.error) {
                fbLastData = JSON.stringify(response);
                console.log("Delete user app object failed: " + fbLastData);
                // fbConditionTag.xxx = tag_;
                // fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnDeleteUserAppObjectFailed, fbInst);
            } else {
                fbLastData = JSON.stringify(response);
                console.log("Delete user app object successful: " + fbLastData);
                // fbConditionTag.xxx = tag_;
                // fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnDeleteUserAppObjectSuccess, fbInst);
            }
        });
    };

    /* jshint -W004 */

    Acts.prototype.GetFriends = function (filter, resultAs, tag) {
        if (this.runtime.isDomFree || !fbLoggedIn) {
            return;
        }

        fbConditionTag.friends = "";

        var fields = {};
        if (0 === resultAs) { // "Array"
            fields = {
                "fields": "id, installed "
            };
        } else if (1 === resultAs) { // "Event"
            fields = {
                "fields": "id,name,first_name,last_name, installed "
            };
        }

        FB.api('/me/friends', 'GET', fields, function (response) {

            var arr = response["data"];
            var lengthY = 0;
            var lengthZ = 0;

            if (!response || response.error || !arr || arr.length === 0) {
                console.log(response);
                fbLastData = JSON.stringify(response);
                // fbConditionTag.friends = tag;
                // fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnFriendsNotAvailable, fbInst);
            } else {
                log(response);

                if (0 === resultAs) { // "Array"

                    var arrResult = [];
                    var len1 = arr.length;
                    var resultIdx = 0;
                    var ix1;
                    for (ix1 = 0; ix1 < len1; ix1++) {
                        if ((1 === filter && arr[ix1]["installed"]) || 0 === filter) {
                            arrResult[resultIdx] = [];
                            arrResult[resultIdx][0] = [];
                            arrResult[resultIdx][0][0] = arr[ix1]["id"];
                            resultIdx++;
                        }
                    }

                    // If there is something in the array the y and z need to be 1
                    if (arrResult.length === 0) {
                        lengthY = 0;
                        lengthZ = 0;
                    } else {
                        lengthY = 1;
                        lengthZ = 1;
                    }

                    fbLastData = JSON.stringify({
                        "c2array": true,
                        "size": [arrResult.length, lengthY, lengthZ],
                        "data": arrResult
                    });

                    fbConditionTag.friends = tag;
                    fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnFriendsAvailable, fbInst);
                } else if (1 === resultAs) { // "Event"
                    var len = arr.length;
                    for (var i = 0; i < len; i++) {
                        if ((1 === filter && arr[i]["installed"]) || 0 === filter) {
                            fbLastData = JSON.stringify(arr[i]);

                            //TODO: Change this to save in fbFriendUserID, fbFriendFullName etc
                            fbUserID = arr[i]["id"];
                            fbFullName = arr[i]["name"];
                            fbFirstName = arr[i]["first_name"];
                            fbLastName = arr[i]["last_name"];


                            fbConditionTag.friends = tag;
                            fbRuntime.trigger(cr.plugins_.FacebookFE.prototype.cnds.OnFriendsAvailable, fbInst);
                        }
                    }
                }
            }
        });
    };
    /* jshint +W004 */


    pluginProto.acts = new Acts();

    //////////////////////////////////////
    // Expressions
    function Exps() {
    }

    Exps.prototype.FullName = function (ret) {
        ret.set_string(fbFullName);
    };

    Exps.prototype.FirstName = function (ret) {
        ret.set_string(fbFirstName);
    };

    Exps.prototype.LastName = function (ret) {
        ret.set_string(fbLastName);
    };

    Exps.prototype.Score = function (ret) {
        ret.set_int(fbScore);
    };

    Exps.prototype.HiscoreName = function (ret) {
        ret.set_string(fbHiscoreName);
    };

    Exps.prototype.HiscoreUserID = function (ret) {
        ret.set_int(fbHiscoreUserID);
    };

    Exps.prototype.HiscoreRank = function (ret) {
        ret.set_int(fbRank);
    };

    Exps.prototype.UserID = function (ret) {
        // Float because these numbers are insanely huge now!
        ret.set_float(parseFloat(fbUserID));
    };

    Exps.prototype.LastData = function (ret) {
        ret.set_string(fbLastData);
    };

    Exps.prototype.AppReqResponseObjectId = function (ret) {
        ret.set_string(fbAppReqResponseObjectId);
    };

    Exps.prototype.AppReqResponseTo = function (ret) {
        ret.set_string(fbAppReqResponseTo);
    };

    Exps.prototype.AppReqResponseToNumb = function (ret) {
        ret.set_int(fbAppReqResponseTo.length);
    };

    Exps.prototype.UserRequestInstanceId = function (ret) {
        ret.set_string(fbUserRequestInstanceId);
    };

    Exps.prototype.UserRequestObjectId = function (ret) {
        ret.set_string(fbUserRequestObjectId);
    };

    Exps.prototype.UserRequestFromId = function (ret) {
        ret.set_string(fbUserRequestFromId);
    };

    Exps.prototype.UserRequestFromName = function (ret) {
        ret.set_string(fbUserRequestFromName);
    };

    pluginProto.exps = new Exps();

}());