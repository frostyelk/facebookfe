/**
 * Facebook Adapter for Native Facebook implementations
 * Copyright (c) 2014 Frosty Elk AB
 *
 * Supports:
 *        com.phonegap.plugins.facebookconnect 0.10.0
 *
 *  No support for: com.phonegap.plugins.facebookconnect 0.5.1
 *  No support for: com.phonegap.plugins.facebookconnect 0.6.0
 *
 * Created: 2014-07-01 arne.sikstrom@frostyelk.se
 *
 *
 */
/* jshint -W117 */

function FacebookAdapterInit() {
    "use strict";
    var version = '0.0.1.6';
    console.log("FacebookAdapterInit " + version);

    if (typeof (FB) !== "undefined") {
        console.log("FB already exists. Facebook Adapter not installed!");
        return;
    }

    /*
     * Class Facebook Adapter
     */
    function FacebookAdapterCordova() {

        //console.log("Running FB Adapter Constructor");

        // The plugin does not support getAuthResponse() so cache here it instead.
        this.authResponse = {};
    }

    FacebookAdapterCordova.prototype.init = function (params) {
        // Init is done from the plugin level in the config.xml file
        console.log("Doing FB.init:" + JSON.stringify(params));

    };

    FacebookAdapterCordova.prototype._initCordova = function (callback) {
        console.log("Doing FB._initCordova:");
        var twoHoursFromNow = 3600 * 2;

        var initAuthData = {
            "userID": "",
            "accessToken": "",
            "expiresIn": twoHoursFromNow
        };

        var self = this;

        function createAuthData() {
            console.log("FB Adapter Init authData: " + JSON.stringify(initAuthData));

            self.authResponse["userID"] = initAuthData["userID"];
            self.authResponse["accessToken"] = initAuthData["accessToken"];
            self.authResponse["expiresIn"] = initAuthData["expiresIn"];
            callback();
        }

        function fetchUserDataFromAccessToken() {
            facebookConnectPlugin.api("/me", [], function (response) {
                    //console.log("Adapter init /me api success response: " + JSON.stringify(response));
                    initAuthData["userID"] = response["id"];
                    createAuthData();
                },
                function (response) {
                    console.log("Adapter init /me api fail response: " + JSON.stringify(response));
                }
            );
        }

        facebookConnectPlugin.getAccessToken(
            function (token) {
                //console.log("AccessToken OK:" + JSON.stringify(token));
                initAuthData["accessToken"] = token;
                fetchUserDataFromAccessToken();
            },
            function (error) {
                console.log("AccessToken NOK. Error:" + JSON.stringify(error));
                callback();
            }
        );


    };

    FacebookAdapterCordova.prototype.getAuthResponse = function () {
        console.log("Doing FB.getAuthResponse()");
        //		console.log("Doing FB.getAuthResponse(): " + JSON.stringify(this.authResponse));

        if (typeof this.authResponse.accessToken === 'undefined') {
            return null;
        }

        return this.authResponse;
    };

    FacebookAdapterCordova.prototype.getLoginStatus = function (callback, roundtrip) {
        console.log("Doing FB.getLoginStatus");

        var self = this;
        facebookConnectPlugin.getLoginStatus(
            function (status) {
                self.authResponse = status["authResponse"];

                //self.authResponse.accessToken = status["authResponse"]["accessToken"];
                //self.authResponse.expiresIn = status["authResponse"]["expiresIn"];
                //self.authResponse.session_key = status["authResponse"]["session_key"];
                //self.authResponse.sig = status["authResponse"]["sig"];
                //self.authResponse.userID = status["authResponse"]["userID"];

                callback(status);
            },
            function (error) {
                if (roundtrip) {
                    callback({
                        'status': "unknown",
                        'authResponse': self.authResponse ? self.authResponse : {},
                        'error': error
                    });
                } else {
                    callback({
                        'status': typeof self.authResponse.accessToken === 'undefined' ? "unknown" : "connected",
                        'authResponse': self.authResponse ? self.authResponse : {}
                    });
                }
            }
        );
    };

    /*
     FacebookAdapterCordova.prototype.getLoginStatusLegacy = function (callback, roundtrip) {
     console.log("Doing FB.getLoginStatusLegacy");

     var self = this;

     var authReturnObject = {
     'status': 'unknown'
     //            'authResponse': this.authResponse,
     };

     if (typeof this.authResponse !== 'undefined') {
     authReturnObject.authResponse = this.authResponse;
     }

     function doSuccessfulCallback(status) {
     //			console.log("facebookConnectPlugin.getLoginStatus. Native plugin status: " + JSON.stringify(status));

     authReturnObject.status = status["status"];
     authReturnObject.authResponse.accessToken = status["authResponse"]["accessToken"];
     authReturnObject.authResponse.expiresIn = status["authResponse"]["expiresIn"];
     authReturnObject.authResponse.session_key = status["authResponse"]["session_key"];
     authReturnObject.authResponse.sig = status["authResponse"]["sig"];

     // Plugin 0.7.0 returns null in userID
     if (self.authResponse["userID"] && !status["authResponse"]["userID"]) {
     authReturnObject.authResponse.userID = self.authResponse["userID"];
     }

     self.authResponse["userID"] = authReturnObject.authResponse.userID;
     self.authResponse["accessToken"] = authReturnObject.authResponse.accessToken;
     self.authResponse["expiresIn"] = authReturnObject.authResponse.expiresIn;
     self.authResponse["session_key"] = authReturnObject.authResponse.session_key;
     self.authResponse["sig"] = authReturnObject.authResponse.sig;

     //			console.log("facebookConnectPlugin.getLoginStatus. returning from JS adapter: " + JSON.stringify(authReturnObject));
     callback(authReturnObject);
     }

     function doGetLoginStatus(token) {
     facebookConnectPlugin.getLoginStatus(
     function (status) {
     if (roundtrip === true) {
     authReturnObject.authResponse.accessToken = token;
     }
     doSuccessfulCallback(status);
     },
     function (error) {
     authReturnObject.authResponse = null;
     callback(authReturnObject);
     }
     );
     }

     // The facebookConnectPlugin can not handle a null Session for getLoginStatus() so check for token first
     facebookConnectPlugin.getAccessToken(
     function (token) {
     doGetLoginStatus(token);
     },
     function (error) {
     authReturnObject.authResponse = null;
     callback(authReturnObject);
     }
     );

     };
     */


    FacebookAdapterCordova.prototype.login = function (callback, options) {
        console.log("Doing FB.login, Options: " + JSON.stringify(options));

        var loginOptions = [];

        if (typeof (options["scope"]) !== 'undefined') {
            if (options["scope"] !== null) {
                loginOptions = options["scope"].split(",");
            }
        }

        //console.log("Adapter login options: " + JSON.stringify(loginOptions))

        var self = this;
        facebookConnectPlugin.login(loginOptions,
            function (response) {
                console.log("Adapter login success callback executed");

                // console.log("Plugin login input response : " + JSON.stringify(response));
                self.authResponse = response["authResponse"];
                callback(response);

            },
            function (response) {
                console.log("Adapter login fail callback executed");
                console.log("Plugin login fail response : " + JSON.stringify(response));
                callback(response);
            }
        );

    };

    /*

     */
    FacebookAdapterCordova.prototype.api = function (path, method, params, callback) {
        console.log("Doing FB.api");

        // Inputs
        console.log("api path: " + JSON.stringify(path));
        console.log("api method: " + JSON.stringify(method));
        console.log("api params: " + JSON.stringify(params));

        // plugin params
        var requestPath;
        var permissions = [];

        // Options are optional
        if (arguments.length === 2 && typeof method === 'function') {
            console.log("Using short form for FB.api call");
            callback = method;
            method = 'GET';
            params = {};
            requestPath = path + "/?";
        } else {
            if (params["access_token"]) {
                console.log("access_token sent to FB.api");
            }
            requestPath = path + "/?" + jQuery.param(params);
        }

        var ajaxType = method.toUpperCase();

        if (ajaxType === 'POST' || ajaxType === 'DELETE') {
            // POST and DELETE are not supported by the plugin, do an AJAX call instead
            var url = 'https://graph.facebook.com' + path;

            if (!params["access_token"] && typeof this.authResponse.accessToken !== 'undefined') {
                console.log("No access token sent at POST/DELETE, adding one.");
                params.access_token = this.authResponse.accessToken;
            }

            jQuery.ajax({
                "type": ajaxType,
                "url": url,
                "data": params,
                "success": function (response) {
                    console.log("jQuery.ajax post api success response: " + JSON.stringify(response));
                    callback(response);
                },
                "error": function (response) {
                    console.log("jQuery.ajax post api error response: " + JSON.stringify(response));
                    callback(response);
                }
            });

        } else {
            facebookConnectPlugin.api(requestPath, permissions, function (response) {
                    console.log("Adapter api success callback executed");
                    console.log("Adapter api success response: " + JSON.stringify(response));
                    callback(response);
                },
                function (response) {
                    console.log("Adapter api fail callback executed");
                    console.log("Adapter api fail response: " + JSON.stringify(response));
                    callback(response);
                }
            );
        }

    };


    FacebookAdapterCordova.prototype.ui = function (params, callback) {
        console.log("Doing FB.ui");

        // Inputs
        console.log("ui params: " + JSON.stringify(params));

        facebookConnectPlugin.showDialog(params, function (response) {
                console.log("Adapter showDialog success callback executed");
                console.log("Adapter showDialog success response: " + JSON.stringify(response));
                callback(response);
            },
            function (response) {
                console.log("Adapter showDialog fail callback executed");
                console.log("Adapter showDialog fail response: " + JSON.stringify(response));

                var errorResponse = {};
                if (response === 'undefined') {
                    errorResponse = {
                        'error_code': 9999,
                        'error_message': "Undefined error"
                    };
                } else {
                    errorResponse = {
                        'error_code': response["errorCode"],
                        'error_message': response["errorMessage"]
                    };
                }
                callback(errorResponse);
            }
        );
    };

    FacebookAdapterCordova.prototype.logout = function (callback) {
        console.log("Doing FB.logout");
        var self = this;
        facebookConnectPlugin.logout(function (response) {
                console.log("Adapter logout success callback executed");
                self.authResponse = {};
                callback(response);
            },
            function (response) {
                console.log("Adapter logout fail callback executed");
                callback(response);
            }
        );
    };

    FacebookAdapterCordova.prototype.Event = {};

    FacebookAdapterCordova.prototype.Event.subscribe = function (/*event, callback*/) {
        console.log("FB.Event.subscribe not supported");
    };


    FacebookAdapterCordova.prototype.Event.unsubscribe = function (/*event, callback*/) {
        console.log("FB.Event.unsubscribe not supported");
    };


    if (typeof cordova !== 'undefined' && typeof facebookConnectPlugin !== 'undefined') {
        console.log("Setting up Facebook Adapter for Cordova");
        window.FB = new FacebookAdapterCordova();
    } else {
        console.log("No supported native FB framework available");
    }
}